﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DoctorsController : ControllerBase
    {       
        [HttpGet("{expression:alpha}/{number:int}")]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Get))]
        public async Task<ActionResult<DoctorList>> GetDoctors(string expression, int number)
        {
            var sortExpression = new SqlParameter("@sortExpression", expression);
            var pageNumber = new SqlParameter("@pageNumber", number);

            using (var db = new HospitalContext())
            {
                return Ok(await Task.FromResult(
                db.DoctorLists.FromSqlRaw(
                    "GetDoctorsList @sortExpression, @pageNumber", sortExpression, pageNumber)
                .ToList()
                ));
            }
        }

        [HttpGet("{id:int}")]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Get))]
        public async Task<ActionResult<int>> GetDoctor(int id)
        {
            using (var db = new HospitalContext())
            {
                var doctor = await db.Doctors.FirstOrDefaultAsync(p => p.Id == id);
                if (doctor == null)
                {
                    return NotFound("Doctor not found");
                }
                var doctorEditable = new DoctorEditable()
                {
                    Id = doctor.Id,
                    CabinetId = doctor.CabinetId,
                    RegionsId = doctor.RegionsId,
                    SpecializationId = doctor.SpecializationId
                };
                return Ok(doctorEditable);
            };
        }

        [HttpPost]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Post))]
        public async Task<ActionResult<Doctor>> PostDoctor([FromBody] Doctor doctor)
        {
            if (doctor == null)
            {
                return BadRequest("The doctor is null"); ;
            }
            using (var db = new HospitalContext())
            {
                try
                {
                    db.Doctors.Add(doctor);
                    await db.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    return BadRequest($"Error adding doctor {ex.Message}");
                }
            }
            return NoContent();
        }

        [HttpPut]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Put))]
        public async Task<IActionResult> PutDoctor([FromBody] Doctor doctor)
        {
            using (var db = new HospitalContext())
            {
                if (doctor == null || db.Doctors.Where(d => d.Id == doctor.Id).Count() == 0)
                {
                    return BadRequest("Doctor not found or null");
                }
                try
                {
                    db.Update(doctor);
                    await db.SaveChangesAsync();
                    return NoContent();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
        }

        [HttpDelete("{id:int}")]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Delete))]
        public async Task<IActionResult> DeleteDoctor(int id)
        {
            using (var db = new HospitalContext())
            {
                var doctor = await db.Doctors.FirstOrDefaultAsync(p => p.Id == id);
                if (doctor == null)
                {
                    return BadRequest("The doctor not found"); ;
                }
                db.Doctors.Remove(doctor);
                await db.SaveChangesAsync();
                return Ok();
            }
        }
    }
}