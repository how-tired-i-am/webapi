using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PatientsController : ControllerBase
    {
        [HttpGet("{expression:alpha}/{number:int}")]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Get))]
        public async Task<ActionResult<PatientList>> GetPatients(string expression, int number)
        {
            var sortExpression = new SqlParameter("@sortExpression", expression);
            var pageNumber = new SqlParameter("@pageNumber", number);

            using (var db = new HospitalContext())
            {
                return Ok(await Task.FromResult(
                db.PatientLists.FromSqlRaw(
                    "GetPatientsList @sortExpression, @pageNumber", sortExpression, pageNumber)
                .ToList()
                ));
            }
        }

        [HttpGet("{id:int}")]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Get))]
        public async Task<ActionResult<int>> GetPatient(int id)
        {
            using (var db = new HospitalContext())
            {
                var patient = await db.Patients.FirstOrDefaultAsync(p => p.Id == id);
                if (patient == null)
                {
                    return NotFound("Entry not found");
                }
                var patientEditable = new PatientEditable()
                {
                    Id = patient.Id,
                    RegionId = patient.RegionId
                };
                return Ok(patientEditable);
            };
        }

        [HttpPost]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Post))]
        public async Task<ActionResult<Patient>> PostPatient([FromBody] Patient patient)
        {
            if (patient == null)
            {
                return BadRequest("The patient is null"); ;
            }
            using (var db = new HospitalContext())
            {
                try
                {
                    db.Patients.Add(patient);
                    await db.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    return BadRequest($"Error adding entry {ex.Message}");
                }
            }           
            return NoContent();
        }

        [HttpPut]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Put))]
        public async Task<IActionResult> PutPatient([FromBody] Patient patient)
        {
            using (var db = new HospitalContext())
            {
                if (patient == null || db.Patients.Where(p => p.Id == patient.Id).Count() == 0)
                {
                    return BadRequest("Order not found or null");
                }
                try
                {
                    db.Update(patient);
                    await db.SaveChangesAsync();
                    return NoContent();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
        }

        [HttpDelete("{id:int}")]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Delete))]
        public async Task<IActionResult> DeletePatient(int id)
        {
            using (var db = new HospitalContext())
            {
                var patient = await db.Patients.FirstOrDefaultAsync(p => p.Id == id);
                if (patient == null)
                {
                    return BadRequest("The patient not found"); ;
                }
                db.Patients.Remove(patient);
                await db.SaveChangesAsync();
                return Ok();
            }
        }
    }
}