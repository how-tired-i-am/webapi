USE Hospital;
GO

IF NOT EXISTS 
( 
	SELECT TOP 1 1 
	FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'GetPatientsList') AND TYPE IN (N'P', N'PC') 
) 
BEGIN 
PRINT 'Creating procedure dbo.GetPatientsList' 
EXEC ('CREATE PROCEDURE GetPatientsList AS RAISERROR(''Not implemented yet.'', 2004, 10);') 
END 
PRINT 'Altering of procedure dbo.GetPatientsList' 
GO 

ALTER PROCEDURE GetPatientsList(@sortExpression NVARCHAR(100), @pageNumber INT) 
AS 
DECLARE @msql VARCHAR(3000)
DECLARE @offset INT
IF (@pageNumber > 0) 
	SET @offset = (@pageNumber * 5) - 5
ELSE
	SET @offset = 0
BEGIN 
	SET @msql = 'SELECT pt.Id, pt.Surname, pt.Name, pt.MiddleName, pt.Address, pt.DateOfBirth, rg.Number
	FROM dbo.Patients AS pt
	LEFT JOIN dbo.Regions AS rg ON pt.RegionId = rg.Id ORDER BY ' + @sortExpression + ' OFFSET ' + CAST(@offset AS nvarchar) + ' ROWS FETCH NEXT 5 ROWS ONLY'
	EXEC(@msql)
END