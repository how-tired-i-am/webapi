USE Hospital;
GO

IF NOT EXISTS 
( 
	SELECT TOP 1 1 
	FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'GetDoctorsList') AND TYPE IN (N'P', N'PC') 
) 
BEGIN 
PRINT 'Creating procedure dbo.GetDoctorsList' 
EXEC ('CREATE PROCEDURE GetDoctorsList AS RAISERROR(''Not implemented yet.'', 2004, 10);') 
END 
PRINT 'Altering of procedure dbo.GetDoctorsList' 
GO 

ALTER PROCEDURE GetDoctorsList(@sortExpression NVARCHAR(100), @pageNumber INT) 
AS 
DECLARE @msql VARCHAR(3000)
DECLARE @offset INT
IF (@pageNumber > 0) 
	SET @offset = (@pageNumber * 5) - 5
ELSE
	SET @offset = 0
BEGIN 
	set @msql = 'SELECT dc.Id, dc.Name, cb.Number AS CabinetNumber, sp.Name AS Specialization, rg.Number AS RegionNumber 
				FROM Doctors AS dc
				LEFT JOIN Cabinets AS cb ON dc.CabinetId = cb.Id
				LEFT JOIN Specializations AS sp ON dc.SpecializationId = sp.Id
				LEFT JOIN Regions AS rg ON dc.RegionsId = rg.Id 
				ORDER BY ' + @sortExpression + ' OFFSET ' + CAST(@offset AS nvarchar) + ' ROWS FETCH NEXT 5 ROWS ONLY'
	EXEC(@msql)
END