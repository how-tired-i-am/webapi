﻿namespace WebApi
{
    public class DoctorList
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public int CabinetNumber { get; set; }
        public string Specialization { get; set; } = null!;
        public int RegionNumber { get; set; }
    }
}
