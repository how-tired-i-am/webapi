﻿namespace WebApi
{
    public partial class DoctorEditable
    {
        public int Id { get; set; }
        public int CabinetId { get; set; }
        public int SpecializationId { get; set; }
        public int RegionsId { get; set; }
    }
}
