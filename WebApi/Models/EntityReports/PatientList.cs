﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebApi
{
    public class PatientList
    {
        public int Id { get; set; }
        public string Surname { get; set; } = null!;
        public string Name { get; set; } = null!;
        public string MiddleName { get; set; } = null!;
        public string Address { get; set; } = null!;
        public DateTime DateOfBirth { get; set; }

        [Column("Number")]
        public int RegionNumber { get; set; }
    }
}
