﻿namespace WebApi
{
    public partial class Cabinet
    {
        public Cabinet()
        {
            Doctors = new HashSet<Doctor>();
        }

        public int Id { get; set; }
        public int Number { get; set; }

        public virtual ICollection<Doctor> Doctors { get; set; }
    }
}
