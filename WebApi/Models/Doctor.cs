﻿namespace WebApi
{
    public partial class Doctor
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public int CabinetId { get; set; }
        public int SpecializationId { get; set; }
        public int RegionsId { get; set; }

        public virtual Cabinet Cabinet { get; set; } = null!;
        public virtual Region Regions { get; set; } = null!;
        public virtual Specialization Specialization { get; set; } = null!;
    }
}
